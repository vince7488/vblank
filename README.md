# VBLANK #
***By*** *Vernard Mercader*
***Version*** *2.5.0*

This is a 'not-so-blank' **Blankslate** loaded with more features so it has some of the common standard scripts and plugins widely used in Web Development.

This blankslate really isn't "blank" at all;

This is pre-loaded with libraries like **jQuery**, and the good thing is it's not just one version of jQuery - it loads a version of jQuery depending whether you're using IE, Chrome, Firefox, etc.

This also uses **Bootstrap's** CSS Framework to build Responsive Designs.

**FontAwesome** is the default font-based CSS iconography library.

**FitText** jQuery is also preloaded so you can easily implement beautifully sized headers and large text.

Lastly, Modernizr JS is also loaded so you have maximum compatibility across browsers.

==============

If you have suggestions on what to make as "defaults" for this not-so-blank blank, then contact me.


Updates:

 - (23 JAN 2017): Updated Footer.php structure and added a default WordPress Navigation bar for header.php.

Blankslate
NakedTheme
Clean
HTML5
