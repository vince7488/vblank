<?php
add_action( 'after_setup_theme', 'v_setup' );
add_theme_support( 'menus' );

function v_setup()	{
	load_theme_textdomain( 'v', get_template_directory() . '/languages' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'html5', array( 'search-form' ) );

	define('V_DIR', get_template_directory_uri());
	define('V_TEMPLATE_DIR', get_template_directory());
	define('THE_SITE', get_site_url());

	global $content_width;

	if ( ! isset( $content_width ) ) $content_width = 640;
		register_nav_menus(
		array( 'main-menu' => __( 'Main Menu', 'v' ) )
	);

	flush_rewrite_rules( false );

	// remove the unwanted <meta> links
  remove_action('wp_head', 'rsd_link');
  remove_action('wp_head', 'wlwmanifest_link');
  remove_action('wp_head', 'wp_generator');
  remove_action('wp_head', 'wp_shortlink_wp_head');
  // remove the X-Pingback header
  function remove_x_pingback($headers) {
    unset($headers['X-Pingback']);
  	return $headers;
  }
  add_filter('wp_headers', 'remove_x_pingback');
}

function v_nav() { /* <== This is what you call to invoke the menu */
	$menuArg = array(
		'theme_location'  => 'main-menu',
		'menu'            => '',
		'container'       => false,
		'container_class' => '',
		'container_id'    => '',
		'menu_class'      => 'nav navbar-nav navbar-right',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
		'depth'           => 0,
		'walker'          => new WP_Bootstrap_Navwalker()
	);

	wp_nav_menu( $menuArg );
}

require_once('includes/bootstrap-navwalker.php');

/*  FOR BOOTSTRAP - add "active" class instead of "current-menu-item" */
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item) {
	if( in_array('current-menu-item', $classes) ) {
		$classes[] = ' active ';
	}
	return $classes;
}

add_action( 'wp_enqueue_scripts', 'v_load_scripts' );

function v_load_scripts()
{
	/* Registers */

		/* jQuery picker by browser */
		// This functions.php has a capability of loading scripts depending on Browser type
		//FOR MORE MODERNITY, FEEL FREE TO UPDATE JQUERY AND BOOTSTRAP TO THE MOST COMPATIBLE VERSION
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		wp_deregister_script('jquery');
		// If IE or older ones like netscape
		if(
			( preg_match('/MSIE/i',$user_agent) && !preg_match('/Opera/i',$user_agent) ) ||
			(preg_match('/Netscape/i',$user_agent))
			):
			wp_register_script( 'jquery', V_DIR . '/js/jquery-1.11.3.min.js', array(), '1.11.3', false );
			wp_enqueue_script( 'jquery' );

		//if Mozilla or Chrome/Safari or Opera
		elseif(
			( preg_match('/mozilla/i',$user_agent) && !preg_match('/compatible/', $user_agent) ) ||
			( preg_match('/webkit/i',$user_agent) ) ||
			(preg_match('/Opera/i',$user_agent))
		):
			wp_register_script( 'jquery', V_DIR . '/js/jquery-3.3.1.min.js', array(), '3.3.1', false );
			wp_register_script( 'jquery-ui', V_DIR . '/js/jquery-ui/jquery-ui.min.js', array(), '1.12.1', true );
			wp_enqueue_script( 'jquery' );
			wp_enqueue_script( 'jquery-ui' );

		else:
			//load local WP copy of jQuery
			wp_register_script('jquery', '/wp-includes/js/jquery/jquery.js', false, '1.3.2', false);
			wp_enqueue_script( 'jquery' );
		endif;

		/* ...continue registers */

/* > SCRIPTS */
		wp_register_script( 'angular', '//ajax.googleapis.com/ajax/libs/angularjs/1.7.2/angular.min.js', '1,7,2', true);
		wp_register_script( 'fittext', V_DIR . '/js/jquery.fittext.js', array(), '1.2.0', false );
		wp_register_script( 'modernizr', V_DIR . '/js/modernizr-custom.js', array(), '2.8', true );
		wp_register_script( 'bootstrap', V_DIR . '/js/bootstrap.bundle.min.js', array(), '4.1.3', true );
		wp_register_script( 'lity', V_DIR . '/js/lity.min.js', array(), '2.3.1', true );
		wp_register_script( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array(), '1.8.1', true );
		wp_register_script( 'masonry', V_DIR . '/js/masonry.pkgd.min.js', array(), '4.2.2', true );

/* > STYLES */
		wp_register_style( 'bootstrap', V_DIR . '/css/bootstrap.min.css', '', '4.1.3', 'screen' );
		wp_register_style( 'bs-reboot', V_DIR . '/css/bootstrap-reboot.min.css', '', '4.1.3', 'screen' );
		wp_register_style( 'bs-grid', V_DIR . '/css/bootstrap-grid.min.css', '', '4.1.3', 'screen' );
		//wp_register_style( 'googlefonts', '//fonts.googleapis.com/css?family=' ); //uncomment this if you've figured out what fonts you like to use
		wp_register_style( 'fa', '//use.fontawesome.com/releases/v5.3.1/css/all.css', '', '5.3.1', 'screen' );
		wp_register_style( 'ion', '//unpkg.com/ionicons@4.4.2/dist/css/ionicons.min.css', '', '4.4.2', 'screen' );
		wp_register_style( 'lity', V_DIR . '/css/lity.min.css', '', '2.3.1', 'screen' );
		wp_register_style( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', '', '1.8.1', 'screen' );

		wp_register_style( 'vfg', V_DIR . '/css/vflexgrid.min.css', '', '1.0.0', 'screen' );

		/* Enqueue */
		wp_enqueue_script( 'fittext' ); //FitText
		wp_enqueue_script( 'modernizr' ); //Modernizr
		wp_enqueue_script( 'bootstrap' ); //Bootstrap JS
		wp_enqueue_script( 'angular' ); //Vernard being bold here diving into the sea with very little swimming skills
		wp_enqueue_script( 'lity' ); //Best Lightbox script

		wp_enqueue_style( 'bootstrap' ); //bootstrap CSS
		wp_enqueue_style( 'bs-reboot' ); //bootstrap CSS
		wp_enqueue_style( 'bs-grid' ); //bootstrap CSS
		wp_enqueue_style( 'fa' ); //FontAwesome
		wp_enqueue_style( 'ion' ); //FontAwesome
		wp_enqueue_style( 'lity' ); //Lity
		wp_enqueue_style( 'vfg' ); //Flexgrid
}

// =============================== FUNCTIONS ============================= //
//Function to check if site is running on localhost
//Useful when debugging, using other file/function/code, or removing security enhancements when in localhost.
function serverIsLocal() {
	$isLocal = false;
	$url = $_SERVER['HTTP_HOST'];
	$checkLocal = strpos($url,'localhost');
	$checkHome = strpos($url,'127.0.0.1');
	$checkAcCom = strpos($url,'actioncoach');

	if ( ($checkLocal !== false) || ($checkHome !== false) || ($checkAcCom === false) ) {
		$isLocal = true;
	}

	return $isLocal;
}

//in case you have a staging environment
/* function serverIsStaging() {
  $isStaging = false;
  $url = $_SERVER['HTTP_HOST'];
  $domain = 'actioncoach.com';
  $staging = 'staging.actioncoach';
  $checkAcCom = strpos($url,$domain);
  $checkStaging = strpos($url,$staging);

  if ( ($checkAcCom !== false) && ($checkStaging !== false) ) {
    $isStaging = true;
  }

  return $isStaging;
} */

//Shorten a string, especially good for turning everything into Excerpts
function shortenIt( $str,$len ) {
	$shortened = substr($str,0,$len) . '&hellip;';

	return strip_tags($shortened);
}

//Real way to check if the_content() is empty... because &nbsp isn't something, it's nothing.
function empty_content($str) {
    return trim(str_replace('&nbsp;','',strip_tags($str,'<img>'))) == '';
}

//REMOVE ALL EMPTY <p>&nbsp;</p> on wp_content
add_filter( 'the_content', 'remove_empty_p', 20, 1 );
function remove_empty_p( $content ) {
	// clean up p tags around block elements
	$content = preg_replace( array(
		'#<p>\s*<(div|aside|section|article|header|footer)#',
		'#</(div|aside|section|article|header|footer)>\s*</p>#',
		'#</(div|aside|section|article|header|footer)>\s*<br ?/?>#',
		'#<(div|aside|section|article|header|footer)(.*?)>\s*</p>#',
		'#<p>\s*</(div|aside|section|article|header|footer)#',
	), array(
		'<$1',
		'</$1>',
		'</$1>',
		'<$1$2>',
		'</$1',
	), $content );
	return preg_replace('#<p>(\s|&nbsp;)*+(<br\s*/*>)*(\s|&nbsp;)*</p>#i', '', $content);
}

//Real way to check if Featured Image really has a post thumbnail url
//based on issue: https://wordpress.org/support/topic/how-to-do-a-real-has_image-check/
function hasThumbnailUrl() {
	global $post;
	$checkThumbnail = get_the_post_thumbnail_url();
	if ( has_post_thumbnail() && $checkThumbnail && (!is_category() || !is_archive() || !is_search() || !is_404()) ) {
		return true;
	}
	else {
		return false;
	}
}

//Grab full address bar URL
function addBarURL() {
    $protocol = (isset($_SERVER['HTTPS']) == true) ? 'https' : 'http';

    return $protocol.'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
}

//Prepare a string to be CSS ID ready
function cleanID($idstring) {
  //strip all tags
  $idstring = wp_strip_all_tags($idstring);
  //Lower case everything
  $idstring = strtolower($idstring);
  //Make alphanumeric (removes all other characters)
  $idstring = preg_replace("/[^a-z0-9_\s-]/", "", $idstring);
  //Clean up multiple dashes or whitespaces
  $idstring = preg_replace("/[\s-]+/", " ", $idstring);
  //Convert whitespaces and underscore to dash
  $idstring = preg_replace("/[\s_]/", "-", $idstring);

  return $idstring;
}

//Proper String Cleanup (SEO ready, content ready)
function cleanDesc($descStr) {
  //remove full word starting with https
  $regexHTTPS = "@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@";
  $descStr = preg_replace($regexHTTPS, ' ', $descStr);
  //remove full word starting with http
  $regexHTTP = "@(http?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@";
  $descStr = preg_replace($regexHTTP, ' ', $descStr);
  //remove full word starting with www
  $regexWWW = "@(www?([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@";
  $descStr = preg_replace($regexWWW, ' ', $descStr);
  $descStr = wp_strip_all_tags($descStr);
	$descStr = htmlspecialchars($descStr);

  return $descStr;
}

//Proper Structure for Web page titling.
//Use in place of "get_the_title()" function
function htmlTitle() {
	if (is_category()) {
		$htmlTitle = 'Category: '.get_the_title('').' - ';

	} elseif (function_exists('is_tag') && is_tag()) {
		$htmlTitle = single_tag_title('Tag Archive for &quot;').'&quot; - ';

	} elseif (is_archive()) {
		$htmlTitle = get_the_title('').' Archive - ';

	} elseif (is_page()) {
		$htmlTitle = get_the_title('').' - ';

	} elseif (is_search()) {
		$htmlTitle = 'Search for &quot;'.get_search_query().'&quot; - ';

	} elseif (!(is_404()) && (is_single()) || (is_page())) {
		$htmlTitle = get_the_title('').' - ';

	} elseif (is_404()) {
		$htmlTitle = 'Not Found - ';

	} else {
		$htmlTitle = get_bloginfo('description').' - ';
	}
	//standard title suffix
	$htmlTitle = $htmlTitle." ".get_bloginfo('name');

	return cleanDesc($htmlTitle);
}

//BREADCRUMBS
function custom_breadcrumbs() {
  $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
  $delimiter = '<span class="fa fa-arrow-right"></span>'; // delimiter between crumbs
  $home = 'Home'; // text for the 'Home' link
  $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
  $before = '<span class="current">'; // tag before the current crumb
  $after = '</span>'; // tag after the current crumb

  global $post;
  $homeLink = get_bloginfo('url');

  if (is_home() || is_front_page()) {

    if ($showOnHome == 1) echo '<div id="crumbs"><a href="' . $homeLink . '" rel="bookmark" role="navigation">' . $home . '</a></div>';

  } else {

    echo '<div id="crumbs"><a href="' . $homeLink . '" rel="bookmark" role="navigation">' . $home . '</a> ' . $delimiter . ' ';

    if ( is_category() ) {
      $thisCat = get_category(get_query_var('cat'), false);
      if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
      echo $before . 'Archive by category "' . single_cat_title('', false) . '"' . $after;

    } elseif ( is_search() ) {
      echo $before . 'Search results for "' . get_search_query() . '"' . $after;

    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '" rel="bookmark" role="navigation">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '" rel="bookmark" role="navigation">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;

    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '" rel="bookmark" role="navigation">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;

    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;

    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/" rel="bookmark" role="navigation">' . $post_type->labels->singular_name . '</a>';
        if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        if ($showCurrent == 0) $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
        echo $cats;
        if ($showCurrent == 1) echo $before . get_the_title() . $after;
      }

    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;

    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo '<a href="' . get_permalink($parent) . '" rel="bookmark" role="navigation">' . $parent->post_title . '</a>';
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;

    } elseif ( is_page() && !$post->post_parent ) {
      if ($showCurrent == 1) echo $before . get_the_title() . $after;

    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '" rel="bookmark" role="navigation">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      for ($i = 0; $i < count($breadcrumbs); $i++) {
        echo $breadcrumbs[$i];
        if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';
      }
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;

    } elseif ( is_tag() ) {
      echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;

    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $before . 'Articles posted by ' . $userdata->display_name . $after;

    } elseif ( is_404() ) {
      echo $before . 'Error 404' . $after;
    }

    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }

    echo '</div>';

  }
} // end custom_breadcrumbs()

//PAGINATION
function custom_pagination($numpages = '', $pagerange = '', $paged='') {

  if ( empty($pagerange) ) {
    $pagerange = 2;
  }

  global $paged;
  if ( empty($paged) ) {
    $paged = 1;
  }
  if ( $numpages == '' ) {
    global $wp_query;

		$numpages = $wp_query->max_num_pages;

    if(!$numpages) {
        $numpages = 1;
    }
  }

  $pagination_args = array(
    'base'            => get_pagenum_link(1) . '%_%',
    'format'          => '/page/%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => False,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => __('<span class="fa fa-chevron-left"></span>'),
    'next_text'       => __('<span class="fa fa-chevron-right"></span>'),
    'type'            => 'plain',
    'add_args'        => false,
    'add_fragment'    => ''
  );

  $paginate_links = paginate_links( $pagination_args );

  if ($paginate_links) {
    echo "<div class='pagination' role='navigation'>";
      echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
      echo $paginate_links;
    echo "</div>";
  }
} //end PAGINATION

// Add new rewrite rule to add /blogs to blog posts *ONLY*
//This makes sure that "domain.com/a-very-long-blog-post" is always redirecting to "domain.com/blog/a-very-long-blog-post"
/* UNCOMMENT IF NEEDED */
/* function create_new_url_querystring() {
    add_rewrite_rule(
        'blog/([^/]*)$',
        'index.php?name=$matches[1]',
        'top'
    );
    add_rewrite_tag('%blog%','([^/]*)');
}
add_action('init', 'create_new_url_querystring', 999 );

  ////Modify post link
  ////This will print /blog/post-name instead of /post-name
 function append_query_string( $url, $post, $leavename ) {
	if ( $post->post_type == 'post' ) {
		$url = home_url( user_trailingslashit( "blog/$post->post_name" ) );
	}
	return $url;
}
add_filter( 'post_link', 'append_query_string', 10, 3 );

  ////Redirect all posts to new url
  ////If you get error 'Too many redirects' or 'Redirect loop', then delete everything below
function customPerms() {
	if ( is_singular('post') ) {
		global $post;
		if ( strpos( $_SERVER['REQUEST_URI'], '/blog/') === false) {
		   wp_redirect( home_url( user_trailingslashit( "blog/$post->post_name" ) ), 301 );
		   exit();
		}
	}
}
add_filter( 'template_redirect', 'customPerms' ); */


//Comments handler
add_action( 'comment_form_before', 'v_enqueue_comment_reply_script' );
function v_enqueue_comment_reply_script()
{
if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}
add_filter( 'the_title', 'v_title' );
function v_title( $title ) {
if ( $title == '' ) {
return '&rarr;';
} else {
return $title;
}
}
add_filter( 'wp_title', 'v_filter_wp_title' );
function v_filter_wp_title( $title )
{
return $title . esc_attr( get_bloginfo( 'name' ) );
}
add_action( 'widgets_init', 'v_widgets_init' );
function v_widgets_init()
{
register_sidebar( array (
'name' => __( 'Sidebar Widget Area', 'v' ),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => "</li>",

'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}
function v_custom_pings( $comment )
{
$GLOBALS['comment'] = $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php
}
