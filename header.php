<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="vblank-body" class="hfeed">

		<!-- Default Bootstrap Menu.  Cheap, but worth it. -->
		<nav class="navbar navbar-default" id="v-nav" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse.navbar-collapse" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="fa fa-bars"></span>
		      </button>

		      <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home">
				<?php echo esc_html( get_bloginfo( 'name' ) ); ?>
		      </a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse">

			<form class="navbar-form navbar-right" id="searchform" role="search" method="get" autocomplete="off" action="<?php echo home_url( '/' ); ?>">
			   <div class="form-group">
			     <input type="search" class="form-control search-field" placeholder="Search …" value="" name="s">
			   </div>
			   <button type="submit" id="searchsubmit" class="btn btn-default">Submit</button>
			</form>

			<?php
			if ( has_nav_menu( 'main-menu' ) ) {
			     v_nav(); //invoke Menu. See functions.php
				 /*
				To make the menu work you will need
				to create the menu in WordPress
				Admin via Appearance > Menus.
				Create the menu, and set the Menu
				Location (usually "Main Menu").
				 */
			}
			else {
				echo "<div class=\"nav navbar-nav navbar-right\"><span style=\"padding: 15px;display: block;position: relative;color: #c5c5c5;\">The Menu is not setup yet.</span></div>";
			}
			?>

		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>

		<header id="header" role="banner">
			<section id="branding">
				<div id="site-title">
				<?php
				if ( is_front_page() || is_home() || is_front_page() && is_home() )
				{
					echo '<h1>';
				}
				?>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home">
					<?php echo esc_html( get_bloginfo( 'name' ) ); ?>
				</a>
				<?php
				if ( is_front_page() || is_home() || is_front_page() && is_home() )
				{
					echo '</h1>';
				}
				?>
				</div>

				<div id="site-description">
					<?php bloginfo( 'description' ); ?>
				</div>
			</section>
		</header>

		<div id="container">
<?php //continue off some content, end at footer.php ?>
